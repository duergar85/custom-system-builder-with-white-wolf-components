module.exports = {
    'Create Actor': '.actors-sidebar button.create-document',
    'Actor name': '//input[@name="name"]',
    'Actor type': '//select[@name="type"]',
    'Create New Actor': '.dialog-button.ok',

    'Save Component': 'button.dialog-button.validate',
    'Delete Component': 'button.dialog-button.delete',
    'Cancel Component Edition': 'button.dialog-button.cancel'
};
